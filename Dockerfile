FROM ubuntu:latest

RUN apt-get update \
  && apt-get install -y curl
RUN curl -fsSL https://deno.land/x/install/install.sh | sh
RUN mv ~/.deno/bin/deno /usr/local/bin/deno
